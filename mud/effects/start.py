from .effect import Effect2, Effect3
from mud.events import StartOnEvent, StartOffEvent

class StartOnEffect(Effect2):
    EVENT = StartOnEvent

class StartOffEffect(Effect3):
    EVENT = StartOffEvent


