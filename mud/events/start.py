from .event import Event3, Event2

class StartOnEvent(Event2):
    NAME = "start-on"

    def perform(self):
        if not self.object.has_prop("startable"):
            self.fail()
            return self.inform("start-on.failed")
        self.inform("start-on")


class StartOffEvent(Event2):
    NAME = "start-off"

    def perform(self):
        if not self.object.has_prop("startable"):
            self.fail()
            return self.inform("start-off.failed")
        self.inform("start-off")



class StartWithEvent(Event3):
    NAME = "start-with"

    def perform(self):
        if not self.object.has_prop("startable"):
            self.fail()
            return self.inform("start-with.failed")
        self.inform("start-with")
