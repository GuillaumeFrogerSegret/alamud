from .event import Event2

class CleanEvent(Event2):
    NAME = "clean"

    def perform(self):
        if not self.object.has_prop("cleanable"):
            self.fail()
            return self.inform("clean.failed")
        self.inform("clean")
