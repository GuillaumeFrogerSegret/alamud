from .event import Event2

class WatchEvent(Event2):
    NAME = "watch"

    def perform(self):
        if not self.object.has_prop("watchable"):
            self.fail()
            return self.inform("watch.failed")
        self.inform("watch")
