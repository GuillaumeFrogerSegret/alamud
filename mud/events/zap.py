from .event import Event3

        
class ZapWithEvent(Event3):
    NAME = "zap-with"

    def perform(self):
        if not self.object.has_prop("zappable"):
            self.fail()
            return self.inform("zap-with.failed")
        self.inform("zap-with")
