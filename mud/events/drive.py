from .event import Event2

class DriveEvent(Event2):
    NAME = "drive"

    def perform(self):
        if not self.object.has_prop("drivable"):
            self.fail()
            return self.inform("drive.failed")
        self.inform("drive")
