from .action import Action2
from mud.events import CleanEvent

class CleanAction(Action2):
    EVENT = CleanEvent
    RESOLVE_OBJECT = "resolve_for_use"
    ACTION = "clean"
