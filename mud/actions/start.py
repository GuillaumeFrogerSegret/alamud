from .action import Action3, Action2
from mud.events import StartWithEvent, StartOnEvent, StartOffEvent

class StartOnAction(Action2):
    EVENT = StartOnEvent
    ACTION = "start-on"
    RESOLVE_OBJECT = "resolve_for_use"

class StartOffAction(Action2):
    EVENT = StartOffEvent
    ACTION = "start-off"
    RESOLVE_OBJECT = "resolve_for_use"

class StartWithAction(Action3):
    EVENT = StartWithEvent
    ACTION = "start-with"
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
