from .action import Action2
from mud.events import WatchEvent

class WatchAction(Action2):
    EVENT = WatchEvent
    RESOLVE_OBJECT="resolve_for_take"
    ACTION = "watch"
