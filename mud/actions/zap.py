from .action import Action3
from mud.events import ZapWithEvent


class ZapWithAction(Action3):
    EVENT = ZapWithEvent
    ACTION = "zap-with"
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
