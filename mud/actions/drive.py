from .action import Action2
from mud.events import DriveEvent

class DriveAction(Action2):
    EVENT = DriveEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "drive"